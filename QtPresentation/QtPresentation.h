#pragma once

#include <QApplication>
#include <QMainWindow>
#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QColor>

#include <map>
#include <string>
#include <array>
#include <memory>
#include <functional>

using Listener = std::function<void()>;

class QtPresentation
{
public:
    QtPresentation(int& argc, char* argv[], std::string name,
                   unsigned rows, unsigned columns, unsigned cellSize, QColor color);
    ~QtPresentation();

    int exec();

    void setColor(unsigned row, unsigned column, QColor color);

    void setKeyListener(int key, Listener listener);
    int startTimer(int durationMS, Listener listener);
    void stopTimer(int timerId);

private:
    using Application = QApplication;
    class MainWindow : public QMainWindow
    {
    public:
        using Listeners = std::map<int, Listener>;

        Listeners keyListeners;
        Listeners timerHandlers;

    protected:
        void keyPressEvent(QKeyEvent * event) override;
        void timerEvent(QTimerEvent *event);
    };
    using CentralWidget = QWidget;
    using MainLayout = QGridLayout;
    using Label = QLabel;
    using LabelRow = std::vector<std::unique_ptr<Label>>;
    using LabelMatrix = std::vector<LabelRow>;

    Application application;
    MainWindow mainWindow;
    CentralWidget centralWidget;
    MainLayout layout;
    LabelMatrix labelMatrix;
};
