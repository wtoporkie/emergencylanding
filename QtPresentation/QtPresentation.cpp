#include "QtPresentation.h"
#include <QPalette>
#include <QKeyEvent>
#include <QTimerEvent>
#include <map>

void QtPresentation::MainWindow::keyPressEvent(QKeyEvent *event)
{
    auto it = keyListeners.find(event->key());
    if (it != keyListeners.end())
    {
        it->second();
    }
}

void QtPresentation::MainWindow::timerEvent(QTimerEvent *event)
{
    auto it = timerHandlers.find(event->timerId());
    if (it != timerHandlers.end())
    {
        it->second();
    }
}

QtPresentation::QtPresentation(int &argc, char* argv[],
                               std::string name,
                               unsigned rows, unsigned columns, unsigned cellSize,
                               QColor color)
 : application(argc, argv),
   mainWindow(),
   centralWidget(&mainWindow),
   layout(&centralWidget),
   labelMatrix(rows)
{
    mainWindow.setFixedSize(cellSize * columns, cellSize * rows);
    mainWindow.setWindowTitle(QString::fromStdString(name));

    int l_row = 0, l_column = 0;
    for (auto&& row: labelMatrix)
    {
        row.resize(columns);
        for (auto& cell: row)
        {
            cell.reset(new Label());
            cell->setAlignment(Qt::AlignCenter);
            cell->setFixedSize(cellSize, cellSize);
            cell->setFrameStyle(QFrame::NoFrame | QFrame::Plain);
            cell->setAutoFillBackground(true);
            layout.addWidget(cell.get(), l_row, l_column);
            setColor(l_row, l_column, color);
            ++l_column;
        }
        ++l_row;
        l_column = 0;
    }
    mainWindow.setCentralWidget(&centralWidget);
}

QtPresentation::~QtPresentation()
{}

int QtPresentation::exec()
{
    mainWindow.show();
    return application.exec();
}

void QtPresentation::setColor(unsigned row, unsigned column, QColor color)
{
    auto& label = *(labelMatrix[row][column]);

    QPalette palette = label.palette();
    palette.setColor(QPalette::Background, color);
    //palette.setColor(QPalette::Foreground, color);
    label.setPalette(palette);

    label.setText("");
}

void QtPresentation::setKeyListener(int key, Listener listener)
{
    mainWindow.keyListeners[key] = std::move(listener);
}

int QtPresentation::startTimer(int durationMS, Listener listener)
{
    int timerId = mainWindow.startTimer(durationMS);
    mainWindow.timerHandlers[timerId] = std::move(listener);
    return timerId;
}

void QtPresentation::stopTimer(int timerId)
{
    mainWindow.killTimer(timerId);
    mainWindow.timerHandlers.erase(timerId);
}

