#include "QtGame.hpp"


int main(int argc, char* argv[])
{
    const Position columns = 80;
    const Position rows = 40;
    const Position cellSize = 10;

    QtGrid::Colors colors{};
    colors.sky = Qt::white;
    colors.ground = Qt::darkGreen;
    colors.building = Qt::blue;

    colors.aircraft = Qt::black;
    colors.bomb = Qt::darkRed;
    colors.rocket = Qt::red;
    colors.crashedAircraft = Qt::darkYellow;

    QtPresentation presentation(argc, argv, "Chernobyl landing", rows, columns, cellSize, colors.sky);
    QtGame game(presentation, rows, columns, colors);

    game.makeCity();
    game.addAircraft();

    return presentation.exec();
}


