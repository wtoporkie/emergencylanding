#include "QtGrid.hpp"
#include <string>
#include <stdexcept>


QtGrid::QtGrid(QtPresentation &presentation,
               IGrid& baseGrid,
               const Colors &colors)
    : presentation(presentation),
      baseGrid(baseGrid),
      colors(colors)
{}

void QtGrid::setCell(const Position2D &p, Cell cell)
{
    baseGrid.setCell(p, cell);
    presentation.setColor(y(p), x(p), colors.getCellColor(cell));
}

IGrid::Cell QtGrid::getCell(const Position2D &p) const
{
    return baseGrid.getCell(p);
}

QColor QtGrid::Colors::getCellColor(Cell cell) const
{
#define CASE_COLOR(color) case Cell::color: return color
    switch (cell)
    {
    CASE_COLOR(sky);
    CASE_COLOR(ground);
    CASE_COLOR(building);
    CASE_COLOR(aircraft);
    CASE_COLOR(bomb);
    CASE_COLOR(rocket);
    CASE_COLOR(crashedAircraft);
    default:
        throw std::logic_error("wrong cell type: " + std::to_string(static_cast<int>(cell)));
    }
}

