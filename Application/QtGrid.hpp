#pragma once

#include "QtPresentation.h"
#include "IGrid.hpp"

class QtGrid : public IGrid
{
public:
    struct Colors
    {
        QColor sky;
        QColor ground;
        QColor building;
        QColor aircraft;
        QColor bomb;
        QColor rocket;
        QColor crashedAircraft;
        QColor getCellColor(IGrid::Cell) const;
    };

    QtGrid(QtPresentation& presentation, IGrid& baseGrid, Colors const& colors);

    Cell getCell(Position2D const& p) const override;
    void setCell(Position2D const& p, Cell) override;

private:
    QtPresentation& presentation;
    IGrid& baseGrid;
    Colors colors;
};

