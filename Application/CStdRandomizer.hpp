#pragma once

#include "IRandomizer.hpp"
#include <random>

class CStdRandomizer : public IRandomizer
{
public:
    CStdRandomizer();
    Position rand(Size elements) override;

private:
    // std::random_device randomDevice; - not work in gcc
    std::mt19937 randomEngine;
    using Distribution = std::uniform_int_distribution<Position>;
};

