#include "QtGame.hpp"
#include <algorithm>

#include "Bomb.hpp"
#include "Rocket.hpp"

QtGame::QtGame(QtPresentation &presentation,
               Size rows, Size columns,
               const QtGrid::Colors &colors)
    : presentation(presentation),
      grid(rows, columns),
      qtGrid(presentation, grid, colors),
      randomizer(),
      rows(rows),
      columns(columns),
      initialDuration(100),
      moveDuration(initialDuration),
      //
      aircraft(qtGrid, columns, rows)
{
    setGround();

    presentation.setKeyListener(Qt::Key_Escape, [this] { reset(); });
    presentation.setKeyListener(Qt::Key_PageUp, [this] { speedUp(); });
    presentation.setKeyListener(Qt::Key_PageDown, [this] { speedDown(); });
    presentation.setKeyListener(Qt::Key_Return, [this] { flipTimer(); });
    presentation.setKeyListener(Qt::Key_Pause, [this] { flipTimer(); });
}

QtGame::~QtGame()
{
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
    }
}

auto QtGame::generateNumberOfBuildings() -> Size
{
    Size minNumOfBuildings = columns * 2 / 3;
    Size maxNumOfBuildings = columns * 5 / 6;

    return minNumOfBuildings + randomizer.rand(maxNumOfBuildings - minNumOfBuildings);
}

void QtGame::paintBuilding(Size position, Size hight)
{
    for(auto i=2u; i<hight+2; ++i)
    {
        qtGrid.setCell(Position2D{position, rows - i}, IGrid::Cell::building);
    }
}

void QtGame::makeCity()
{
    // TODO
    std::vector<Size> positions(columns);
    std::iota(positions.begin(), positions.end(), 0);
    std::shuffle(positions.begin(), positions.end(), std::mt19937(std::time(0)));

    Size numOfBuildings = generateNumberOfBuildings();
    Size highestBuilding = rows * 7 / 10;
    Size numOfFloors = highestBuilding * numOfBuildings * 3 / 9;

    for (Size i = 0; i < numOfBuildings and numOfFloors > 0; ++i)
    {
        auto higth = randomizer.rand(highestBuilding);
        if(numOfFloors - higth < 0)
        {
            higth = numOfFloors;
        }

        paintBuilding(positions[i], higth);
        numOfFloors -= higth;
    }
}

void QtGame::addAircraft(Qt::Key bomb, Qt::Key rocket)
{
    //  #  #
    // #####
    Path aircraftBody{{columns - 5, 1},
                      {columns - 4, 1}, {columns - 4, 0},
                      {columns - 3, 1},
                      {columns - 2, 1},
                      {columns - 1, 1}, {columns - 1, 0}};

    aircraft.init(std::move(aircraftBody),
                  Position2D{columns - 3, 1},
                  Direction{-1, 0});
    aircraft.paint();

    presentation.setKeyListener(bomb, [this] { dropBomb(); });
    presentation.setKeyListener(rocket, [this] { fireRocket(); });
}

void QtGame::addMisile(std::unique_ptr<IMisile> misile)
{
    if (misile->startOff(aircraft))
    {
        misiles.emplace_back(std::move(misile));
    }
}

void QtGame::dropBomb()
{
    if (numOfBombs++ == maxNumOfBombs)
    {
        --numOfBombs;
        return;
    }

    addMisile(std::make_unique<Bomb>(qtGrid, columns, rows, [this]{ --numOfBombs; }));
}

void QtGame::fireRocket()
{
    addMisile(std::make_unique<Rocket>(qtGrid, columns, rows));
}

void QtGame::reset()
{
    // TODO - something might be missing here
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
        moveTimer = std::nullopt;
    }

    restartScreen();
    makeCity();
    addAircraft();
    misiles.erase(misiles.begin(), misiles.end());
}

void QtGame::move()
{
    // TODO
    if (not aircraft.move())
    {
        presentation.stopTimer(*moveTimer);
        moveTimer = std::nullopt;
        misiles.erase(misiles.begin(), misiles.end());
        return;
    }

    misiles.erase(
       std::remove_if(misiles.begin(), misiles.end(),
           [](auto& misile)
           {
              return not misile->move();
           }),
       misiles.end());
}

void QtGame::speedUp()
{
    int newDuration = moveDuration * 2;
    if (newDuration > 1000)
        newDuration = 1000;
    if (newDuration != moveDuration)
    {
        moveDuration = newDuration;
        if (moveTimer)
        {
            presentation.stopTimer(*moveTimer);
            moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
        }
    }
}

void QtGame::speedDown()
{
    int newDuration = moveDuration / 2;
    if (newDuration < 10)
        newDuration = 10;
    if (newDuration != moveDuration)
    {
        moveDuration = newDuration;
        if (moveTimer)
        {
            presentation.stopTimer(*moveTimer);
            moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
        }
    }
}

void QtGame::flipTimer()
{
    if (moveTimer)
    {
        presentation.stopTimer(*moveTimer);
        moveTimer = std::nullopt;
    }
    else
    {
        moveTimer = presentation.startTimer(moveDuration, [this] { move(); });
    }
}

void QtGame::setGround()
{
    for (Position c = 0; c < columns; ++c)
    {
        qtGrid.setCell(Position2D(c, rows - 1), IGrid::Cell::ground);
    }
}

void QtGame::restartScreen()
{
    for (Position r = 0; r < rows - 1; ++r)
    {
        for (Position c = 0; c < columns; ++c)
        {
            qtGrid.setCell(Position2D(c, r), IGrid::Cell::sky);
        }
    }
}
