#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <functional>

#include "Grid.hpp"

using namespace testing;

class GridTestSuite : public Test
{
protected:

    const Size ROWS = 3;
    const Size COLUMNS = 2;
    const Size ALL_CELLS = ROWS * COLUMNS;

    Grid objectUnderTest{ROWS, COLUMNS};
};

TEST_F(GridTestSuite, shallHaveAllSkyCellsAfterConstruction)
{
    for (Position row = 0; row < ROWS; ++row)
        for (Position col = 0; col < COLUMNS; ++col)
            ASSERT_EQ(objectUnderTest.getCell(Position2D(col, row)), IGrid::Cell::sky) << col << " " << row;
}

TEST_F(GridTestSuite, shallChangeCellType)
{
    const Position2D POS(1, 2);
    const auto CELL = IGrid::Cell::aircraft;

    objectUnderTest.setCell(POS, CELL);
    ASSERT_EQ(CELL, objectUnderTest.getCell(POS));
}
