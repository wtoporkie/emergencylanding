#include <gtest/gtest.h>
#include <gmock/gmock.h>

#include <functional>

#include "Aircraft.hpp"
#include "Bomb.hpp"
#include "Rocket.hpp"
#include "GridMock.hpp"

using namespace testing;

namespace
{
    constexpr Size COLUMNS = 3;
    constexpr Size ROWS = 4;

    Position2D TOPLEFT{0,0};
    Position2D TOPMIDDLE{1,0};
    Position2D TOPRIGHT{2,0};

    Position2D MIDDLELEFT{0,1};
    Position2D MIDDLE{1,1};
    Position2D MIDDLERIGHT{2,1};

    Position2D BOTTOMMIDDLE{1,2};
    Position2D BOTTOMMRIGHT{2,2};
    Position2D ONTHEGROUND{0,3};

    Vector2D TOLEFT{-1, 0};
    Vector2D TORIGHT{1, 0};

    StrictMock<GridMock> gridMock;
}

void expectPaint(Position2D one, IGrid::Cell cellType)
{
    EXPECT_CALL(gridMock, setCell(one, cellType)).RetiresOnSaturation();
}

void expectPaint(Position2D one, Position2D two, IGrid::Cell cellType)
{
    expectPaint(one, cellType);
    expectPaint(two, cellType);
}


/////////////////////////////////////////////////////////////////////////////////////
/// AircraftTestSuite
/////////////////////////////////////////////////////////////////////////////////////

struct AircraftTestSuite : public Test
{
    AircraftTestSuite() : aircraft(gridMock, COLUMNS, ROWS)
    {
        EXPECT_CALL(gridMock, getCell(_))
            .WillRepeatedly(Return(IGrid::Cell::sky));
    }

    Aircraft aircraft;
};

TEST_F(AircraftTestSuite, shouldMove)
{
    aircraft.init(Path{TOPRIGHT, MIDDLE},
                         std::move(TOPRIGHT),
                         std::move(TOLEFT));

    expectPaint(TOPRIGHT, MIDDLE, IGrid::Cell::sky);
    expectPaint(TOPMIDDLE, MIDDLELEFT, IGrid::Cell::aircraft);

    ASSERT_TRUE(aircraft.move());
}

TEST_F(AircraftTestSuite, shouldTurnBackAndLowerDownWhenLeftBorderHit)
{
    aircraft.init(Path{TOPRIGHT, MIDDLE},
                         std::move(TOPRIGHT),
                         std::move(TOLEFT));

    expectPaint(TOPRIGHT, MIDDLE, IGrid::Cell::sky);
    expectPaint(TOPMIDDLE, MIDDLELEFT, IGrid::Cell::aircraft);
    expectPaint(TOPMIDDLE, MIDDLELEFT, IGrid::Cell::sky);
    expectPaint(MIDDLELEFT, BOTTOMMIDDLE, IGrid::Cell::aircraft);

    ASSERT_TRUE(aircraft.move());
    ASSERT_TRUE(aircraft.move());
}

TEST_F(AircraftTestSuite, shouldTurnBackAndLowerDownWhenRightBorderHit)
{
    aircraft.init(Path{TOPMIDDLE, MIDDLERIGHT},
                         std::move(TOPRIGHT),
                         std::move(TORIGHT));

    expectPaint(TOPMIDDLE, MIDDLERIGHT, IGrid::Cell::sky);
    expectPaint(MIDDLERIGHT, BOTTOMMIDDLE, IGrid::Cell::aircraft);

    ASSERT_TRUE(aircraft.move());
}

TEST_F(AircraftTestSuite, shouldLand)
{
    aircraft.init(Path{MIDDLE, BOTTOMMRIGHT},
                         std::move(TOPRIGHT),
                         std::move(TORIGHT));

    expectPaint(MIDDLE, BOTTOMMRIGHT, IGrid::Cell::sky);
    expectPaint(MIDDLE, BOTTOMMRIGHT, IGrid::Cell::aircraft);

    ASSERT_FALSE(aircraft.move());
}

TEST_F(AircraftTestSuite, shouldCrash)
{
    aircraft.init(Path{MIDDLELEFT, BOTTOMMIDDLE},
                         std::move(TOPRIGHT),
                         std::move(TORIGHT));

    EXPECT_CALL(gridMock, getCell(BOTTOMMRIGHT)).
        WillOnce(Return(IGrid::Cell::building));

    expectPaint(MIDDLELEFT, BOTTOMMIDDLE, IGrid::Cell::sky);
    expectPaint(MIDDLE, BOTTOMMRIGHT, IGrid::Cell::crashedAircraft);

    ASSERT_FALSE(aircraft.move());
}

TEST_F(AircraftTestSuite, shouldDropBomb)
{
    Bomb bomb(gridMock, COLUMNS, ROWS, []{});
    Position2D BOMB{2,1};

    aircraft.init(Path{}, std::move(TOPLEFT), std::move(TOLEFT));

    expectPaint(BOMB, IGrid::Cell::bomb);

    ASSERT_TRUE(aircraft.fire(&bomb));
}

TEST_F(AircraftTestSuite, shouldNotBeAbleToDropBombFromTheGround)
{
    Bomb bomb(gridMock, COLUMNS, ROWS, []{});

    aircraft.init(Path{}, std::move(BOTTOMMIDDLE), std::move(TOLEFT));

    ASSERT_FALSE(aircraft.fire(&bomb));
}

TEST_F(AircraftTestSuite, shouldFireRocket)
{
    Rocket rocket(gridMock, COLUMNS+1, ROWS);
    Position2D ROCKET{3,0};

    aircraft.init(Path{}, std::move(TOPLEFT), std::move(TORIGHT));

    expectPaint(ROCKET, IGrid::Cell::rocket);

    ASSERT_TRUE(aircraft.fire(&rocket));
}

TEST_F(AircraftTestSuite, shouldNotFireRocketBeyondBorder)
{
    Rocket rocket(gridMock, COLUMNS, ROWS);

    aircraft.init(Path{}, std::move(TOPLEFT), std::move(TORIGHT));

    ASSERT_FALSE(aircraft.fire(&rocket));
}

/////////////////////////////////////////////////////////////////////////////////////
/// BombTestSuite
/////////////////////////////////////////////////////////////////////////////////////

struct BombTestSuite : public Test
{
    Size maxNumOfBombs = 1;
    std::unique_ptr<Bomb> bomb = std::make_unique<Bomb>(gridMock, COLUMNS, ROWS,
                                       [this]{ --maxNumOfBombs; });
};

TEST_F(BombTestSuite, shouldBeDroppedFromtTheAir)
{
    expectPaint(MIDDLE, IGrid::Cell::bomb);
    ASSERT_TRUE(bomb->startOff(MIDDLE));
}

TEST_F(BombTestSuite, shouldNotBeDroppedFromTheGround)
{
    ASSERT_FALSE(bomb->startOff(ONTHEGROUND));
}

TEST_F(BombTestSuite, shouldMove)
{
    expectPaint(MIDDLE, IGrid::Cell::bomb);
    expectPaint(MIDDLE, IGrid::Cell::sky);
    expectPaint(BOTTOMMIDDLE, IGrid::Cell::bomb);

    ASSERT_TRUE(bomb->startOff(MIDDLE));
    ASSERT_TRUE(bomb->move());
}

TEST_F(BombTestSuite, shouldNotDigIntoTheGround)
{
    expectPaint(MIDDLE, IGrid::Cell::bomb);
    expectPaint(MIDDLE, IGrid::Cell::sky);
    expectPaint(BOTTOMMIDDLE, IGrid::Cell::bomb);
    expectPaint(BOTTOMMIDDLE, IGrid::Cell::sky);

    ASSERT_TRUE(bomb->startOff(MIDDLE));
    ASSERT_TRUE(bomb->move());
    ASSERT_FALSE(bomb->move());
}

TEST_F(BombTestSuite, shouldPerfomCelanupUponDestruction)
{
    bomb.reset();
    ASSERT_EQ(maxNumOfBombs, 0);
}

/////////////////////////////////////////////////////////////////////////////////////
/// RocketTestSuite
/////////////////////////////////////////////////////////////////////////////////////

struct RocketTestSuite : public Test
{
    RocketTestSuite() : rocket(gridMock, COLUMNS+1, ROWS)
    {
    }

    Rocket rocket;
};

TEST_F(RocketTestSuite, shouldBeFired)
{
    expectPaint(MIDDLE, IGrid::Cell::rocket);

    ASSERT_TRUE(rocket.startOff(MIDDLE, TOLEFT));
}

TEST_F(RocketTestSuite, shouldNotBeFiredbBeyondTheRightEdge)
{
    Vector2D OFFRIGHTEDGE{COLUMNS+1, 0};

    ASSERT_FALSE(rocket.startOff(OFFRIGHTEDGE, TORIGHT));
}

TEST_F(RocketTestSuite, shouldNotGoBeyondTheRightEdge)
{
    Vector2D RIGHTEDGE{COLUMNS, 0};
    expectPaint(RIGHTEDGE, IGrid::Cell::rocket);
    expectPaint(RIGHTEDGE, IGrid::Cell::sky);

    ASSERT_TRUE(rocket.startOff(RIGHTEDGE, TORIGHT));
    ASSERT_FALSE(rocket.move());
}

TEST_F(RocketTestSuite, shouldNotBeFiredBeyondTheLeftEdge)
{
    Vector2D OFFLEFTEDGE{-1, 0};
    ASSERT_FALSE(rocket.startOff(OFFLEFTEDGE, TOLEFT));
}

TEST_F(RocketTestSuite, shouldNotGoBeyondTheLeftEdge)
{
    expectPaint(TOPLEFT, IGrid::Cell::rocket);
    expectPaint(TOPLEFT, IGrid::Cell::sky);

    ASSERT_TRUE(rocket.startOff(TOPLEFT, TOLEFT));
    ASSERT_FALSE(rocket.move());
}

TEST_F(RocketTestSuite, shouldHaveLimitedRange)
{
    expectPaint(TOPLEFT, IGrid::Cell::rocket);

    expectPaint(TOPLEFT, IGrid::Cell::sky);
    expectPaint(TOPMIDDLE, IGrid::Cell::rocket);
    expectPaint(TOPMIDDLE, IGrid::Cell::sky);

    ASSERT_TRUE(rocket.startOff(TOPLEFT, TORIGHT));
    ASSERT_FALSE(rocket.move());
}



