#include <gmock/gmock.h>
#include <gtest/gtest.h>
#include "IGrid.hpp"
class GridMock : public IGrid
{
public:
    GridMock();
    ~GridMock() override;
    MOCK_CONST_METHOD1(getCell, Cell(Position2D const&));
    MOCK_METHOD2(setCell, void(Position2D const&, Cell));
};
