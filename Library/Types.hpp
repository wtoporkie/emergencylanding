#pragma once

#include <cstdint>
#include <tuple>
#include <deque>

using Position = std::uint16_t;
using Position2D = std::tuple<Position,Position>;
using Size = Position;
using Size2D = Position2D;
using Direction = Position2D;

using Coordinate = std::int16_t;
using Vector2D = std::tuple<Coordinate,Coordinate>;
using Path = std::deque<Vector2D>;

inline Coordinate& x(Vector2D& p) { return std::get<0>(p); }
inline Coordinate const& x(Vector2D const& p) { return std::get<0>(p); }
inline Coordinate& y(Vector2D& p) { return std::get<1>(p); }
inline Coordinate const& y(Vector2D const& p) { return std::get<1>(p); }

inline Vector2D operator + (Vector2D const& lhs, Vector2D const& rhs)
{
    return Vector2D{x(lhs) + x(rhs), y(lhs) + y(rhs)};
}

inline Vector2D operator - (Vector2D const& lhs, Vector2D const& rhs)
{
    return Vector2D{x(lhs) - x(rhs), y(lhs) - y(rhs)};
}

inline Vector2D operator * (Coordinate a, Vector2D const& rhs)
{
    return Vector2D{a*x(rhs), a*y(rhs)};
}
