#pragma once

#include "IMisile.hpp"

class IGrid;

class Rocket : public IMisile
{
public:
    Rocket(IGrid&, Position, Position);

    bool startOff(Aircraft&);
    bool move() override;

    bool startOff(const Vector2D&, const Vector2D&);

private:
    IGrid& _grid;
    Position _cols;
    Position _rows;
    Vector2D _position;
    Vector2D _direction;
    Coordinate _start;
};


