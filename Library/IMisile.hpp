#pragma once

#include "Types.hpp"

class Aircraft;

struct IMisile
{
    virtual bool startOff(Aircraft&) = 0; // Shoud accept some interface like 'MisileLouncher'...
    virtual bool move() = 0;
    virtual ~IMisile() = default;
};
