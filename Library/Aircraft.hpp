#pragma once

#include "Types.hpp"
#include "IGrid.hpp"
#include "IMisile.hpp"

#include <memory>

class Bomb;
class Rocket;

class Aircraft
{
public:

    Aircraft(IGrid&, Position, Position);

    auto init(Path&& body, Vector2D&&, Vector2D&&) -> void;
    auto paint(IGrid::Cell cell = IGrid::Cell::aircraft) -> void;
    auto move() -> bool;

    // Make it an interface...
    bool fire(Bomb*);
    bool fire(Rocket*);

private:

    void moveBody(const Direction&);
    void turnBack();
    bool crashed();
    bool hitsBorder();
    bool hasLanded();
    bool canMove();

    Path _body;
    IGrid& _grid;
    Position _cols;
    Position _rows;
    Vector2D _direction{-1,0};
    Vector2D _weaponDock;
    Coordinate _minX;
    Coordinate _maxX;
    Coordinate _bottom;
};
