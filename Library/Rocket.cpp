#include "Rocket.hpp"
#include "Aircraft.hpp"
#include "IGrid.hpp"

Rocket::Rocket(IGrid& grid, Position cols, Position rows)
      : _grid(grid), _cols(cols), _rows(rows)
{
}

bool Rocket::startOff(Aircraft& airctraft)
{
    return airctraft.fire(this);
}

bool Rocket::startOff(const Vector2D& position, const Vector2D& direction)
{
    if(x(position) >= _cols or x(position) < 0)
    {
        return false;
    }

    _position = position;
    _direction = direction;
    _start = x(position);
    _grid.setCell(Position2D{x(_position), y(_position)},
                  IGrid::Cell::rocket);
    return true;
}

bool Rocket::move()
{
    for (int i=0; i<2; i++)
    {
        _grid.setCell(Position2D{x(_position), y(_position)},
                      IGrid::Cell::sky);

        auto newPosition = _position + _direction;

        if(x(newPosition) >= _cols or x(newPosition) < 0)
        {
            return false;
        }
        if(std::abs(x(newPosition) - _start) >= _cols/2 )
        {
            return false;
        }

        _position = newPosition;

        _grid.setCell(Position2D{x(_position), y(_position)},
                      IGrid::Cell::rocket);
    }

    return true;
}
