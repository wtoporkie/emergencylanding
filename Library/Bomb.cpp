#include "Bomb.hpp"
#include "Aircraft.hpp"
#include "IGrid.hpp"

Bomb::Bomb(IGrid& grid, Position cols, Position rows, std::function<void()>&& cleanup )
    : _grid(grid), _cols(cols), _rows(rows), _cleanup(std::move(cleanup))
{
}

Bomb::~Bomb()
{
    _cleanup();
}

bool Bomb::startOff(Aircraft& aircraft)
{
    return aircraft.fire(this);
}

bool Bomb::startOff(const Vector2D& position)
{
    if (y(position) >= _rows-1)
    {
        return false;
    }

    _position = position;
    _grid.setCell(Position2D{x(_position), y(_position)},
                  IGrid::Cell::bomb);

    return true;
}

bool Bomb::move()
{
    _grid.setCell(Position2D{x(_position), y(_position)},
                  IGrid::Cell::sky);

    auto pos = _position + Vector2D{0, 1};
    if(y(pos) >= _rows-1)
    {
        return false;
    }
    _position = pos;

    _grid.setCell(Position2D{x(_position), y(_position)},
                  IGrid::Cell::bomb);

    return true;
}
