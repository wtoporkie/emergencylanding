#include "Grid.hpp"

Grid::Grid(const Size p_rows, const Size p_columns)
{
    // In the beginning developer created the heaven
    m_cells.resize(p_rows, Row(p_columns, Cell::sky));
}

void Grid::setCell(const Position2D &p, Cell cell)
{
    m_cells.at(y(p)).at(x(p)) = cell;
}

IGrid::Cell Grid::getCell(const Position2D &p) const
{
    return m_cells.at(y(p)).at(x(p));
}
