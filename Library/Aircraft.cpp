#include "Aircraft.hpp"
#include "Bomb.hpp"
#include "Rocket.hpp"

#include <algorithm>
#include <iostream>

constexpr Direction DOWN{0,1};

Aircraft::Aircraft(IGrid& grid, Position cols, Position rows)
        : _grid(grid), _cols(cols), _rows(rows)
{
}

void Aircraft::init(Path&& body, Vector2D&& weaponDock, Vector2D&& direction)
{
    _body = std::move(body);
    _weaponDock = std::move(weaponDock);
    _direction = std::move(direction);

    _minX = _cols;
    _maxX = 0;
    _bottom = 0;
    std::for_each(_body.begin(), _body.end(),
    [this](const auto& pos)
    {
        if (x(pos) < _minX) {
            _minX = x(pos);
        }
        if (x(pos) > _maxX) {
            _maxX = x(pos);
        }
        if (y(pos) > _bottom)
        {
            _bottom = y(pos);
        }
    });

    std::cout << "Start: minX/maxX: " << _minX << ":" << _maxX << std::endl;
}

bool Aircraft::move()
{
    paint(IGrid::Cell::sky);

    auto canMoveOn = canMove();

    if (crashed())
    {
        paint(IGrid::Cell::crashedAircraft);
        return false;
    }

    paint(IGrid::Cell::aircraft);

    return canMoveOn;
}

bool Aircraft::canMove()
{
    if (hitsBorder())
    {
        if (hasLanded())
        {
            return false;
        }
        turnBack();
        moveBody(DOWN);
        return true;
    }

    moveBody(_direction);
    return true;
}

bool Aircraft::hitsBorder()
{
    if (x(_direction) == 0)
        return true;

    return (_maxX + x(_direction) >= _cols or
            _minX + x(_direction) < 0);
}

bool Aircraft::hasLanded()
{
    return (_bottom == _rows - 2);
}

bool Aircraft::crashed()
{
    return std::any_of(_body.begin(), _body.end(),
        [this](auto& pos)
        {
            return _grid.getCell(pos) == IGrid::Cell::building;
        });
}

void Aircraft::moveBody(const Direction& direction)
{
    std::transform(_body.begin(), _body.end(), _body.begin(),
       [direction](const auto& pos)
       {
           return pos + direction;
       });

    _minX += x(direction);
    _maxX += x(direction);
    _bottom += y(direction);
    _weaponDock = _weaponDock + direction;
}

void Aircraft::turnBack()
{
    std::transform(_body.begin(), _body.end(), _body.begin(),
       [this](const auto& pos)
       {
           return Vector2D{_maxX + _minX - x(pos), y(pos)};
       });

    x(_direction) = -x(_direction);
}

void Aircraft::paint(IGrid::Cell cell)
{
   std::for_each(_body.begin(), _body.end(),
       [this, cell](const auto& pos)
       {
           _grid.setCell(Position2D{x(pos), y(pos)}, cell);
       });
}

bool Aircraft::fire(Bomb* bomb)
{
    auto position = _weaponDock + Direction{0, 1} - 2*_direction;

    return bomb->startOff(position);
}

bool Aircraft::fire(Rocket* rocket)
{
    auto position = _weaponDock + 3*_direction;

    return rocket->startOff(position, _direction);
}
