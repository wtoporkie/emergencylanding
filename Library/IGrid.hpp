#pragma once

#include "Types.hpp"

class IGrid
{
public:
    virtual ~IGrid() = default;

    enum class Cell
    {
        sky,
        ground,
        building,
        aircraft,
        bomb,
        rocket,
        crashedAircraft
    };

    virtual Cell getCell(Position2D const& p) const = 0;
    virtual void setCell(Position2D const& p, Cell) = 0;
};

