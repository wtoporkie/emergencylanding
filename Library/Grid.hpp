#pragma once

#include "IGrid.hpp"
#include <vector>

class Grid : public IGrid
{
public:
    Grid(const Size p_rows, const Size p_columns);

    void setCell(Position2D const& p, Cell cell) override;
    Cell getCell(Position2D const& p) const override;

private:
    using Row = std::vector<Cell>;
    using Matrix = std::vector<Row>;
    Matrix m_cells;
};

