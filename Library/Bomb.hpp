#pragma once

#include "IMisile.hpp"
#include <functional>

class IGrid;

class Bomb : public IMisile
{
public:
    Bomb(IGrid&, Position, Position, std::function<void()>&&);

    bool startOff(Aircraft&) override;
    bool move() override;

    bool startOff(const Vector2D&);

    ~Bomb() override;

private:
    IGrid& _grid;
    Position _cols;
    Position _rows;
    Position2D _position;
    std::function<void()> _cleanup;
};
