PATH=
PATH=C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\iCLS\;C:\Program Files\Intel\Intel(R) Management Engine Components\iCLS\;C:\ProgramData\Oracle\Java\javapath;C:\Program Files (x86)\RSA SecurID Token Common;C:\WINDOWS\system32;C:\WINDOWS;C:\WINDOWS\System32\Wbem;C:\WINDOWS\System32\WindowsPowerShell\v1.0\;C:\Program Files (x86)\SafeCom\SafeComPrintClient;C:\Program Files\TortoiseSVN\bin;C:\Program Files\PuTTY\;C:\Program Files (x86)\Webex\Webex\Applications;C:\Program Files (x86)\Sennheiser\SoftphoneSDK\;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files\Intel\Intel(R) Management Engine Components\DAL;C:\Program Files (x86)\Intel\Intel(R) Management Engine Components\IPT;C:\Program Files\Intel\Intel(R) Management Engine Components\IPT;C:\Program Files\CMake\bin;C:\Users\toporkie\AppData\Local\Microsoft\WindowsApps

# Game story

## Player

You are pilot of the military aircraft, that has to perform emergency landing in Chernobyl.

The only flat surface suitable for landing is full of old buildings that you have to destroy before landing.
Buildings have different number of floors. 

You are flying in circle above that flat surface. In each circle you are one floor lower than in the previous one. 

## Bombs

![Look at Doc/Bombing.png](Doc/Bombing.png)

You have the unlimited number of bombs. 
But there is one limit: only 5 bombs can be currently active.
The bomb destroys single building entirely.
The bomb flies straight down - no horizontal move.

## Rockets

![Look at Doc/Rocket.png](Doc/Rocket.png)

You have the unlimited number of rockets. Rockets have limited distance around half size of this surface to land on.
Rocket can destroy only one floor. Only one rocket might be active at the given time. The rocket fly in the same manner as aircraft, but are faster.

Actually, both: rockets and bombs are 2x faster than your aircraft.

## Buildings

Each time game is started(restarted(Esc)) - different buildings are generated:

![Look at Doc/Initial1.png](Doc/Initial1.png)
![Look at Doc/Initial1.png](Doc/Initial2.png)

There are several constraints:

  * Highest building - like a little more than half of height of game grid (e.g. rows / 2 + rows / 5)
  * Number of buildings - like a little more than half of width of game grid (e.g. columns / 2 + columns / 6)
  * There is a limit for total number of floors - like 1/3 of what we got if all buildings would be of the highest height.

## Game End

### Crash (failure)

The game ends either with crash (failure):

![Look at Doc/Crash.png](Doc/Crash.png)

### Landing (success)

Or landing (success):

![Look at Doc/Landing.png](Doc/Landing.png)

## Game control

Press Return key to start flying after start/re-start. You can speed down/up by pressing PgUp/PgDn.
Press LeftArrow to throw rocket. Press DownArrow to throw bomb.

Press Pause to stop/start game.

# Real World story

The Easter is coming. Such game, as a gift, is asked for by one of children in your family. You are challenged to write it yourself - to proof your profession skills - this child is not so sure about that ;) 

# More about the project

Take the initial requirements from "Game Story".
Some of them are already implemented - like PgDn/PgUp/Esc/Return - check the existing code.
You have the grid and the randomizer implemented.

Some WoW/DoD requirements:

  * Use git as version control (one of our gitlab serwers)
  * All logic should be unit tested. But do not unit-test Qt/GUI related things. 
    * Unit-test should not block refactoring.
    * Unit-test should test logic - not current implementation.
    * No need for 100% UT coverage - but all important behavior (bombs, rockets, flying, crashing, destroying, landing) should be tested.
  * All logic should be  located in Library directory
  * All UT code should be located in Tests directory
  * Game logic implementation should not have any direct relation to Qt - we plan to have several versions for several "platform" - including semi-graphic.
  * No race conditions! Qt ensures you get only one event (timeout,key-pressed) at the given time - thus rather use single threaded application (main() function thread should be enough).
  * Design/implementation should be "Open-Closed". We plan further development, like:
    * next levels, harder to play (more constraints)
    * some more info displayed - like counters of floors/buildings to destroy etc..
    * some new equipment (stronger rockets) that user will paid for
  * Your goal is to implement the current "Game Story" - nothing more. 
  * Because of time limits - you might: 
    * cut "Game Story" requirements - but the game should be playable (land/crash possible). 
    * Some technical debt (but not lack of UT) is allowed if documented - like duplications, performance problems, lack of encapsulation.
  * All specific design decision, places where game can be "extended", all allowed technical debts should be documented as comments in code or as some separate markdown document. Please use some "TODO", "Place of extending", "Technical debt - duplication/memory leak/performance" comments to point to that places in code or as a chapters in that design document.

# Hints 

## System, compilers etc...

The project was built and run with success on MS Widnwos (7) and Linux (virtual box). 

  * Compiler - gcc7.3 - we need support for C++17
  * CMake - at least version 3.10
  * git (probably any version)
  * Qt - lest least version 5.5 - but just take the newest
  
For MS Windows just take the newest QtCreator (for edit/build/run) with MinGW 64bit  toolkit.

For git on MS Windows - you might use GNU-Bash + Git: https://gitforwindows.org/

Download this version of QtCreator (it worked 3 months ago):
  
  * QtCreator/Qt/MinGW(gcc) https://www.qt.io/download
  * Select Open Source / Windows
  * It is good to have some account in qt
  * There might be a problem with proxy/firewall - there is a -proxy option
  * In ManageKits – if CMake was not detected – specify it manually
  * To speed up MS Windows compilation – try to use -j option (for parallel compilation) - like "-j 8" in "Build Steps"

## Base version of project

You get:

  * Grid/QtGrid imeplemented (rows(y) and columns(x) - Left/Top corner is (0,0), RightBottom is (COLUMNS-1, ROWS-1)
  * CStdRandomizer implemented - for argument (N) it will generate a number from range [0, N-1]
  * Timers and Keyboard events handlers with just specifying lambda for it - see QtGame.cpp
  
  
## Other simialar projects

Whoever attended to my coding-dojo trainings about TDD - will realize at once that this is very similar project to Snake game that we practice on these trainings.
You might copy/use/look-at/whatever you like from these "Snake" implementations:  https://gitlab.dynamic.nsn-net.net//krakow-coding-dojo/QtGridDojoProject/tree/snakegame


  

  






